
#!/usr/bin/env python
__author__      = "Avery Erwin-McGuire"

#  I know I don't need to copyright this, and I feel silly doing it,
#  but I like to keep the habit of having everything under GPL

__copyright__   = "2018, Avery Erwin-McGuire"
__license__ = "GPL"

''' This file fulfills the following requirement:

-----------------------------------------------------------------------------------------------------
                Depth-first searcher

                    a
                / \
                b   c
                /|\   \
                d e f   g
                        |
                        h
            Write code that can search for a node in a tree. 
            Return the node if it exists, and null/nil/none/undefined/false 
            as appropriate in your language if not. 
            For example, if the code is given "g" and a tree with the structure above, 
            it should return the node named "g".

            During your interview, you'll pair on implementing a breadth-first search 
            and/or writing code that works for graphs with cycles.
-----------------------------------------------------------------------------------------------------

Things I notice about this:

Features of the tree:
-> unsorted
-> unbalanced
-> non-binary

Features of the search:
-> return the node, not the value of the node
-> return none if the node is not found

'''

class  Tree:
    """ Tree made of a list in this format: (value, [children]) which supports various searches """

    # NOTE: this does not offer a nice way to build the tree, 
    # it expects the user to provide the tree as a pre-formatted list. 
    # Thinking it over quickly, it might be nice to have something like 
    # "addNode" or etc.
    def __init__(self, treeStructInput):
        self.treeStruct = treeStructInput

    def depthFirstSearch(self,element, tree = "useSelf") :
        '''recursive depth first search'''

        # this is a sloppy fix, I wanted to use tree=self.treeStruct, but self is not defined when the def is interpreted :( 
        # so this just does a quick string literal compare. I didn't bother to look closer into a better way to do this
        # because it worked sufficiently well.
        if tree == "useSelf":
            tree = self.treeStruct
            
        value, children = tree

        #base case 1 - found matching element, (returning node instead of value as per requirement)
        if element == value:
            return tree

        #base case 2 - reached leaf, (returning None as per requirement)
        elif len(children)==0:
            return None
        
        # Recursive statement - didn't match, but have children, check each one
        for child in children:

            result = self.depthFirstSearch(element, child) #recursion

            #if any of the children  matched, pass that up, otherwise return none
            if result!= None:
                return result
            
        #final case - we have children, but none of them and none of their children match
        return None

def main():
    ''' main function which builds a tree and performs a search on it'''

    #tree as per description
    myTree = Tree( ('a',[ (('b'),[('d',[]),('e',[]),('f',[]) ]), ('c', [('g',[ ('h',[]) ]) ]) ]) ) 

    #print result of search for a given value
    print myTree.depthFirstSearch('a')


if __name__ == "__main__":
    main()